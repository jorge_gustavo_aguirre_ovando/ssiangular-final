import {Routes} from '@angular/router';

import {CatalogComponent} from '../catalog/catalog.component';
import {ItemDetailComponent} from '../item-detail/item-detail.component';
import {HomeComponent} from '../home/home.component';
import {AboutComponent} from '../about/about.component';
import {ContactComponent} from '../contact/contact.component';

export const routes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'catalog', component: CatalogComponent},
    {path: 'contactus', component: ContactComponent},
    {path: 'aboutus', component: AboutComponent},
    {path: 'itemdetail/:id', component: ItemDetailComponent},
    {path: '', redirectTo: '/home', pathMatch: 'full'}
];
