import {Injectable} from '@angular/core';

@Injectable()
export class ProcessHttpmsgService {

    constructor() {
    }

    public extractData(res: any) {
        let body = res.body;
        console.log(body);
        return body || {};
    }
}
